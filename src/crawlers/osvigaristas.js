const axios = require('axios');
const cheerio = require('cheerio');
const db = require('../lib/helpers/database_helper')
var last_page = 1
const pegaPiadasPage = () => {
    axios(`https://www.osvigaristas.com.br/piadas/pagina${last_page}.html`).then(response => {
        const html = response.data;
        const $ = cheerio.load(html);
        const articles = $('article');
        articles.each(function () {
            var title = $(this).find('h4').text()
            var joke = $(this).find('.joke').text()
            var category;
            try {
                category = $(this).find('.list-inline').find('li a').attr('href').split('/').slice(-2)[0]
            } catch {
                category = 'Sem Categoria'
            }

            db.query('INSERT INTO piadas (titulo, piada, categoria) VALUES ($1,$2,$3)', [title.trim(), `${joke.trim()}`, category.trim()]).then((s) => {
                console.log('registro inserido')
            }).catch(e => console.error('erro da base', (e)))
        })

        last_page++
        console.log(`Pagina Atua ${last_page}`)
        pegaPiadasPage()
    }).catch(error => {
        console.log('parou em ', error)
    })

}
pegaPiadasPage()
