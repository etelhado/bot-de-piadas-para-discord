const Discord = require("discord.js");
const db = require('../../lib/helpers/database_helper')
class PiadaController {
    enviarPiada = async (message, args) => {
        if (args.length == 0) {
            const piada = await db.query('SELECT piadas.titulo, piadas.piada, piadas.categoria  FROM public.piadas'
                + ' order by random() '
                + ' LIMIT 1')
            const piadaEmbed = new Discord.MessageEmbed()
                .setTitle(piada.rows[0].titulo)
                .setDescription(piada.rows[0].piada)
                .setFooter(`Categoria: ${piada.rows[0].categoria}`)
            await message.channel.send(piadaEmbed)

        } else if (args[0].toLowerCase() == 'categoria') {
            const categorias = await db.query('SELECT array_to_string(array_agg(distinct "categoria"),$$ -|- $$) AS categoria  from piadas')
            message.channel.send(categorias.rows[0].categoria)
        } else if (args.length == 1) {
            const piada = await db.query('SELECT piadas.titulo, piadas.piada, piadas.categoria '
                + 'FROM public.piadas where lower( categoria) = $1 order by random() LIMIT 1',
                [args[0].toLowerCase()])
            const piadaEmbed = new Discord.MessageEmbed()
                .setTitle(piada.rows[0].titulo)
                .setDescription(piada.rows[0].piada)
                .setFooter(`Categoria: ${piada.rows[0].categoria}`)
            await message.channel.send(piadaEmbed)
        }

        console.log(args)
    }
}

module.exports = new PiadaController()