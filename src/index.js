require('dotenv').config()
const PiadaController = require('./app/controllers/piada_controller')
const Discord = require("discord.js");
const client = new Discord.Client();

const prefix = "el#";

client.on("message", async (message) => {
    if (message.author.bot) return;
    if (!message.content.startsWith(prefix)) return;
    if (message.channel.id != process.env.CHANNEL_ID) return;
    const commandBody = message.content.slice(prefix.length);
    const args = commandBody.split(' ');
    const command = args.shift().toLowerCase();

    switch (command) {
        case 'piada':
            await PiadaController.enviarPiada(message, args)
            break;
        case 'frase':
            break;
        default:
            break;
    }

});

client.login(process.env.BOT_TOKEN);



